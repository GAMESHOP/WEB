<?php
use App\Application\Module;
use App\Noctus\Ajax;
use App\Noctus\Constante;
use App\Noctus\Database;
use App\Noctus\Date;
use App\Noctus\Encrypt;
use App\Noctus\Format;
use App\Noctus\Log;
use App\Noctus\Router;
use App\Noctus\sshConnector;

require_once "vendor/autoload.php";
//********************************//
//*********CONFIG NOCTUS *********//
//********************************//
$ajax           = new Ajax();
$constante      = new Constante();
$date           = new Date();
$encrypt        = new Encrypt();
$Format         = new Format();
$router         = new Router();
$log            = new Log();
$ssh            = new sshConnector('srice.eu', '5678', 'site', '1992_Maxime');
if($constante->ENV == 'dev'){
    ini_set('display_errors', 'On');
}else{
    ini_set('display_errors', 'Off');
}
if($constante->SESSION == true){
    session_start();
}
if($constante->DB == true){
    $db = new Database();
}

//********************************//
//*********CONFIG VENDOR *********//
//********************************//


//********************************//
//********* CONFIG APP ***********//
//********************************//