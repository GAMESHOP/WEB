<?php
require_once "core.php";
$_SESSION['module'] = 'HOME';

if(isset($_GET['view'])){
    $view = $_GET['view'];
}else{
    $view = 'index';
}

// Router
ob_start();
if($view === 'index'){require "view/index.php";}
if($view === 'logiciel'){require "view/logiciel.php";}
if($view === 'billetterie'){require "view/billetterie.php";}
if($view === 'apps'){require "view/apps.php";}
if($view === 'assistance'){require "view/assistance.php";}
if($view === 'blog'){require "view/blog.php";}
if($view === 'contact'){require "view/contact.php";}

$content = ob_get_clean();

require "view/template.php";

//END ROUTER