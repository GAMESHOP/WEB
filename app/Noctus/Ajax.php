<?php
/**
 * Created by PhpStorm.
 * User: syltheron
 * Date: 22/11/16
 * Time: 01:56
 */

namespace App\Noctus;


class Ajax
{
    /**
     * Permet de savoir si la requete exécuter est de type AJAX()
     * @return bool
     */
    public function is_ajax(){
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}