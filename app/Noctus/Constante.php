<?php
/**
 * Created by PhpStorm.
 * User: syltheron
 * Date: 22/11/16
 * Time: 02:00
 */

namespace App\Noctus;


class Constante
{
//**************************************//
    //*********CONSTANTE DIRECTION**********//
    //**************************************//

    public $URL       = "gameshop.ovh/"; // Url Sources de la structure domaine.ndl/
    public $ASSETS    = "view/assets/"; // Lien physique vers la structure des fichiers de styles view/assets/
    public $SOURCES   = "https://external.gameshop.ovh/"; // URL direct des ressources Externe au logiciel si elle existes

    //**************************************//
    //************CONSTANTE CORE************//
    //**************************************//

    public $HTTP_PROTOCOL     = "http://"; // Protocol d'accès au service 'http://' ou 'https://'
    public $SESSION           = true; // Prise en charge des variables de sessions ou non (true ou false)
    public $ENV               = "dev"; // Définie le protocole de débugage à adopter par le systeme (Dev affiche les logs et PROD execute normalement)
    public $DB                = true; // Prise en charge de la base de donnée (true or false)

    //**************************************//
    //*******CONSTANTE LOGICIEL ET SITE*****//
    //**************************************//

    public $NOM_SITE          = "GAMESHOP"; // Nom du Site afficher partous
}