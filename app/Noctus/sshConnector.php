<?php
/**
 * Created by PhpStorm.
 * User: vortech
 * Date: 22/11/16
 * Time: 13:29
 */

namespace App\Noctus;


class sshConnector
{
    protected $SERVER   = "";
    protected $PORT     = "";
    protected $USER     = "";
    protected $PASSWORD = "";
    protected $CONNEXION;

    public function __construct($server = null, $port = null, $user = null, $pass = null)
    {
        if($server != null){
            $this->SERVER = $server;
            $this->PORT = $port;
            $this->USER = $user;
            $this->PASSWORD = $pass;
        }
        $connexion = ssh2_connect($this->SERVER, $this->PORT);
        if(ssh2_auth_password($connexion, $this->USER, $this->PASSWORD)){
            ssh2_sftp($connexion);
            return $this->CONNEXION = $connexion;
        }else{
            return new ErrorHandler(300, 'SSH', 'Erreur de Connexion SSH<br>Retour: '.$connexion);
        }
    }

    public function send_file($localFile, $remoteFile){
        $send = ssh2_scp_send($this->CONNEXION, $localFile, $remoteFile);
        if($send == true){
            return true;
        }else{
            return false;
        }
    }
}