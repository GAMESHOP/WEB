<?php
/**
 * Created by PhpStorm.
 * User: vortech
 * Date: 22/11/16
 * Time: 13:27
 */

namespace App\Noctus;


class ErrorHandler
{
    static $code;
    static $sector;
    static $text;


    /**
     * ErrorHandler constructor
     * @param $code // Code Erreur (200: Success / 300: Error / 400: Attention / 500: Information)
     * @param $sector // Secteur de l'erreur (Ex: MYSQL)
     * @param $text // Texte de retour de la requete en erreur
     */
    public function __construct($code, $sector, $text)
    {

        self::$code     = $code;
        self::$sector   = $sector;
        self::$text     = $text;

        $router = new Router();
        return header('Location: '.$router->redirect('view=error&code='.self::$code.'&sector='.self::$sector.'&text='.self::$text));
    }
}