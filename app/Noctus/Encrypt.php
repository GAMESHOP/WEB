<?php
/**
 * Created by PhpStorm.
 * User: vortech
 * Date: 22/11/16
 * Time: 11:50
 */

namespace App\Noctus;


class Encrypt
{
    private static $CYPHER  = MCRYPT_RIJNDAEL_128;
    private static $KEY     = "ENCRYPT NOCTUS 4.0 CYPHERMODE";
    private static $MODE    = "cbc";

    public static function encrypt($data){
        $keyHash    = md5(self::$KEY);
        $key        = substr($keyHash, 0, mcrypt_get_key_size(self::$CYPHER, self::$MODE));
        $iv         = substr($keyHash, 0, mcrypt_get_block_size(self::$CYPHER, self::$MODE));

        $data = implode('', $data);
        $data = mcrypt_encrypt(self::$CYPHER, $key, $data, self::$MODE, $iv);
        return base64_encode($data);
    }

    public static function decrypt($data){
        $keyHash    = md5(self::$KEY);
        $key        = substr($keyHash, 0, mcrypt_get_key_size(self::$CYPHER, self::$MODE));
        $iv         = substr($keyHash, 0, mcrypt_get_block_size(self::$CYPHER, self::$MODE));

        $data = implode('', $data);
        $data = base64_decode($data);
        $data = mcrypt_decrypt(self::$CYPHER, $key, $data, self::$MODE, $iv);
        return rtrim($data);
    }
}