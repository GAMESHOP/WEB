<?php
/**
 * Created by PhpStorm.
 * User: vortech
 * Date: 22/11/16
 * Time: 13:28
 */

namespace App\Noctus;


class Router extends Constante
{
    public function redirect($route = null){
        return $this->HTTP_PROTOCOL.$this->URL.'index.php?'.$route;
    }

    public function getAssets($dos = []){
        return $this->HTTP_PROTOCOL.$this->URL.$this->ASSETS.$this->parseArray($dos);
    }

    public function getSource($dos = []){
        return $this->SOURCES.$this->parseArray($dos);
    }

    public function getRacine(){
        return $this->HTTP_PROTOCOL.$this->URL;
    }

    public function getRedirect($path, $racine = false){
        if($racine == false){
            return $this->HTTP_PROTOCOL.$this->URL.'/view/'.$path;
        }else{
            return $this->HTTP_PROTOCOL.$this->URL.$path;
        }
    }

    public function Controller($path){
        return $this->getRacine().'controller/'.$path;
    }

    private function parseArray($dos){
        return implode('/', $dos);
    }
}