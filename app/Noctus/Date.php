<?php
/**
 * Created by PhpStorm.
 * User: syltheron
 * Date: 22/11/16
 * Time: 02:01
 */

namespace App\Noctus;


class Date
{
    /**
     * @param $date         // Date au format dd-mm-YYYY H:i:s
     * @return false|int    //INT LINUX
     */
    public function strtotime($date){
        return strtotime($date);
    }

    /**
     * @param $format       // Format au choix du dev
     * @param $strt         // Valeur INT LINUX
     * @return false|string // Date au format choisie par le dev
     */
    public function format($format, $strt){
        return date($format, $strt);
    }

    /**
     * @param $strt         // Format INT LINUX
     * @return string       // Retourne la date au format français (Lundi 1 Janvier 1970)
     */
    public function format_fr($strt){
        $j = date('N', $strt);
        $m = date('n', $strt);
        $y = date('Y', $strt);

        $dj = date('d', $strt);

        switch($j){
            case 1: $data_jour = "Lundi"; break;
            case 2: $data_jour = "Mardi"; break;
            case 3: $data_jour = "Mercredi"; break;
            case 4: $data_jour = "Jeudi"; break;
            case 5: $data_jour = "Vendredi"; break;
            case 6: $data_jour = "Samedi"; break;
            case 7: $data_jour = "Dimanche"; break;
        }

        switch($m){
            case 1: $data_mois = "Janvier"; break;
            case 2: $data_mois = "Février"; break;
            case 3: $data_mois = "Mars"; break;
            case 4: $data_mois = "Avril"; break;
            case 5: $data_mois = "Mai"; break;
            case 6: $data_mois = "Juin"; break;
            case 7: $data_mois = "Juillet"; break;
            case 8: $data_mois = "Aout"; break;
            case 9: $data_mois = "Septembre"; break;
            case 10: $data_mois = "Octobre"; break;
            case 11: $data_mois = "Novembre"; break;
            case 12: $data_mois = "Décembre"; break;
        }

        return $data_jour." ".$dj." ".$data_mois." ".$y;
    }

    /**
     * @param $strt         // Format INT LINUX
     * @return float        // Retourne le nombre de jours avant d'arriver à TIME()
     */
    public function reste($strt){
        $diff = $strt - time();
        return round($diff / 86400, 0);
    }

    public function strtActu(){
        return strtotime(date('dmYHis'));
    }
}