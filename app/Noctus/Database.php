<?php
/**
 * Created by PhpStorm.
 * User: syltheron
 * Date: 22/11/16
 * Time: 02:00
 */

namespace App\Noctus;


use PDO;
use PDOException;

class Database
{
    protected $host     = "localhost";
    protected $user     = "root";
    protected $password = "1992_Maxime";
    protected $database = "gameshop";
    private $db;


    public function __construct($host = null, $user = null, $password = null, $database = null)
    {
        if($host != null){
            $this->host     = $host;
            $this->user     = $user;
            $this->password = $password;
            $this->database = $database;
        }

        try{
            $this->db = new PDO('mysql:host='.$this->host.';dbname='.$this->database, $this->user, $this->password, [
                PDO::MYSQL_ATTR_INIT_COMMAND    => "SET NAMES UTF8",
                PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_OBJ
            ]);
        }catch (PDOException $e){
            return new ErrorHandler('300', 'MYSQL', $e->getMessage());
        }
    }

    public function GET($method, $sql, $data = []){
        switch ($method){
            case $method: return $this->$method($sql, $data); break;
            default:
        }
    }

    private function query($sql, $data = []){
        try{
            $req = $this->db->prepare($sql);
            $req->execute($data);
            return $req->fetchAll(PDO::FETCH_OBJ);
        }catch(PDOException $e){
            return new ErrorHandler('300', 'MYSQL', $e->getMessage());
        }
    }

    private function count($sql, $data = []){
        try {
            $req = $this->db->prepare($sql);
            $req->execute($data);
            return $req->fetchColumn();
        }catch(PDOException $e)
        {
            return new ErrorHandler('300', 'MYSQL', $e->getMessage());
        }
    }

    private function execute($sql, $data = []){
        try {
            $req = $this->db->prepare($sql);
            $req->execute($data);
            return $req->rowCount();
        }catch(PDOException $e)
        {
            return new ErrorHandler('300', 'MYSQL', $e->getMessage());
        }
    }
}