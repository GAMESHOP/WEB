<?php
/**
 * Created by PhpStorm.
 * User: vortech
 * Date: 22/11/16
 * Time: 13:28
 */

namespace App\Noctus;


class Format
{
    /**
     * @param $value    // Chiffre avec redondance (0.00)
     * @return string   // Retourne le format suivant: (0,00)
     */
    public function NumberDecimal($value){
        return number_format($value, 2, ',', ' ');
    }

    /**
     * @param $value    // Chiffre avec redondance (0.00)
     * @return string   // Retourne le format suivant: (0,00 €)
     */
    public function Euro($value){
        return number_format($value, 2, ',', ' ')." €";
    }
}