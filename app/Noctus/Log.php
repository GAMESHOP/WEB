<?php
/**
 * Created by PhpStorm.
 * User: syltheron
 * Date: 23/11/16
 * Time: 00:02
 */

namespace App\Noctus;


class Log
{
    protected $database;
    public function __construct()
    {
        $this->database = new Database();
    }

    public function logSuccess($account,$secteur, $desc){
        $this->database->GET("execute", "INSERT INTO log(account, secteur, desc_log, etat_log) VALUES (:account, :secteur, :desc, :etat)", array(
            "account"       => $account,
            "secteur"       => $secteur,
            "desc"          => $desc,
            "etat"          => 3
        ));
        return true;
    }
    public function logInfo($account,$secteur, $desc){
        $this->database->GET("execute", "INSERT INTO log(account, secteur, desc_log, etat_log) VALUES (:account, :secteur, :desc, :etat)", array(
            "account"       => $account,
            "secteur"       => $secteur,
            "desc"          => $desc,
            "etat"          => 2
        ));
        return true;
    }
    public function logWarning($account,$secteur, $desc){
        $this->database->GET("execute", "INSERT INTO log(account, secteur, desc_log, etat_log) VALUES (:account, :secteur, :desc, :etat)", array(
            "account"       => $account,
            "secteur"       => $secteur,
            "desc"          => $desc,
            "etat"          => 1
        ));
        return true;
    }
    public function logError($account,$secteur, $desc){
        $this->database->GET("execute", "INSERT INTO log(account, secteur, desc_log, etat_log) VALUES (:account, :secteur, :desc, :etat)", array(
            "account"       => $account,
            "secteur"       => $secteur,
            "desc"          => $desc,
            "etat"          => 0
        ));
        return true;
    }
}